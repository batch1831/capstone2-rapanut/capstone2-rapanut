const Product = require("../models/Product");

// Create a product
module.exports.addProduct = (reqBody) =>{

	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks
	})

	return newProduct.save().then((product, error) =>{
		if(error){
			return ("An Error has Occured");
		}
		else{
			return ("The Product is Now Saved");
		}
	})
}

// Retrieve all active products
module.exports.getAllActive = () =>{
	return Product.find({isActive: true}).then(result => result);
}

// Retrieve a specific product
module.exports.getProduct = (productId) => {
	return Product.findById(productId).then(result => result);
}

// Update a product details
module.exports.updateProduct = (productId, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Product.findByIdAndUpdate(productId, updatedProduct).then((productUpdate, error) =>{
		if(error){
			return ("An Error has Occured!");
		}
		else{
			return ("The Product Details has been Updated!");
		}
	})
}

// Archive a product

module.exports.archiveProduct = (productId) =>{
	let updateActiveField = {
		isActive : false
	}
	return Product.findByIdAndUpdate(productId, updateActiveField).then((isActive, error)=>{
		if(error){
			return ("An Error Has Occured");
		}
		else{
			return ("Product has been Archived");
		}
	})
}

