const User = require("../models/User");
const Product = require("../models/Product");
const auth = require("../auth");
const bcrypt = require("bcrypt");

// Register a user
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10),
		mobileNo : reqBody.mobileNo
	})
	return newUser.save().then((user, error) => {
		if(error){
			return ("An Error has Occured!");
		}
		else{
			console.log(user);
			return ("User is Now Registered!");
		}
	});
}

// Authenticate a user
module.exports.authenticateUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return ("User Does Not Exist!");
		}

		else{

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				return ("An Error Occured!");
			}
		}
	})
}

// Retrieve user details
module.exports.getProfile = () =>{
	return User.find({}).then(result => result);
}

// Set as admin
module.exports.setAsAdmin = (userId, reqBody) => {

	let updateUserAdmin = {
		isAdmin: reqBody.isAdmin
	}

	return User.findByIdAndUpdate(userId, updateUserAdmin).then((setAsAdmin, error) =>{
		if(error){
			return ("An Error Occured!");
		}
		else{
			return ("User is set to Admin.");
		}
	})
}


// Creating an order
module.exports.checkout = async (data) =>{

	let isUserUpdated = await User.findById(data.userId).then(user =>{

		user.ordered.push({productId: data.productId,
			// quantity: data.quantity,
			totalAmount: data.totalAmount
		})


		return user.save().then((ordered, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	console.log(isUserUpdated);

	let isProductUpdated = await Product.findById(data.productId).then(product =>{


		product.orders.push({userId: data.userId})


		// product.stocks -= totalAmount;


		return product.save().then((orders, error) =>{
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	console.log(isProductUpdated);

	if(isUserUpdated && isProductUpdated){
		return ("Order is Submitted.");
	}

	else{
		return ("Error has Occured.");
	}
}

// Retrieve all orders (admin only)
module.exports.getAllOrdered = () =>{
	return Product.find({order: "Ordered"}).then(result => result);
}

// Retrieve a specific customer's order
module.exports.getMyOrder = (myOrder) =>{
	return Product.find({order: "Ordered"}).then(result => result);
}