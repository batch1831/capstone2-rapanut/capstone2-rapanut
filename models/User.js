const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required!"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required!"]
	},
	email: {
		type: String,
		required: [true, "email is required!"]
	},
	password: {
		type: String,
		required: [true, "Password is required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile No. is required!"]
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	ordered: [
		{
			productId: {
				type: String,
				required: [true, "Product ID is required!"]
			},
			// quantity: {
			// 	type: Number,
			// 	required: [true, "Quantity of order is required!"]
			// },
			totalAmount: {
				type: Number,
				required: [true, "Total Amount is required!"]
			},
			order: {
				type: String,
				default: "Ordered"
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);