// Require Modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

// Creating a server
const app = express();
const port = 3000;

// Connect with MongoDB
mongoose.connect("mongodb+srv://admin:admin@course-booking.go6r6.mongodb.net/e-commerce-app?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Set notification for connection success or failure
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));


// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Routes for our API
app.use("/users", userRoutes);
app.use("/products", productRoutes);

// Listening to port
app.listen(process.env.PORT || port, () =>{
	console.log(`API is now online on port ${process.env.PORT || port}`);
})