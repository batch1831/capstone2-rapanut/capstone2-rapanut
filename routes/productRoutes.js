const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");

// Route for creating a product
router.post("/", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
	productControllers.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send("User is not an Admin!");
	}
})

// Route to retrieve all active products
router.get("/active", (req, res) =>{
	productControllers.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Route to get specific product
router.get("/:productId", (req, res) =>{
	console.log(req.params.productId);

	productControllers.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
})

// Route to update a product details
router.put("/:productId", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
	productControllers.updateProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send("User is not an Admin!");
	}
})

// Route to archive a course
router.put("/:productId/archive", auth.verify, (req,res) =>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
	productControllers.archiveProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send("User is not an Admin!");
	}
})


module.exports = router;