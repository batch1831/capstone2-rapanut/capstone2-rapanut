const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");

// Router for the user registration
router.post("/register", (req, res) =>{
	userControllers.registerUser(req.body).then(resultFromController => res.send( resultFromController));
})

// Route to authenticate user
router.post("/authenticate", (req,res)=>{
	userControllers.authenticateUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for retrieving the user details
router.get("/details", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
	userControllers.getProfile().then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send("You don't have permission on this page!");
	}
})

// Route to set Admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
	userControllers.setAsAdmin(req.params.userId, req.body).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send("You don't have permission on this page!");
	}
})

// Route to create an Order
router.post("/checkout", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization);

	let data = {
		userId: userData.id,
		productId: req.body.productId,
		// quantity: req.body.quantity,
		totalAmount: req.body.totalAmount
	}

	if(userData.isAdmin){
		res.send("You're not allowed to access this page!")
	}
	else{
	userControllers.checkout(data).then(resultFromController => res.send(resultFromController));
	}
})

// Route to retrieve all orders (admin only)
router.get("/orders", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){
	productControllers.getAllActive().then(resultFromController => res.send(resultFromController))
	}
	else{
		res.send("You're not allowed to access this page!")
	}
})

// Route to retrieve a specific customer's orders
router.get("/myOrders", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization)

	let myOrder = {
		userId: userData.id
	}
	
	if(userData.isAdmin){
	res.send("You're not allowed to access this page!")
	}
	else{
		productControllers.getAllActive().then(resultFromController => res.send(resultFromController))
	}
})

module.exports = router;

